-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2018 at 08:25 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentcar`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `Id_Cliente` int(10) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Apellido` varchar(30) DEFAULT NULL,
  `Cedula` varchar(15) DEFAULT NULL,
  `No_Tarjeta` varchar(15) DEFAULT NULL,
  `Límite_Credito` int(50) DEFAULT NULL,
  `Tipo_Persona` varchar(30) DEFAULT NULL,
  `Estado` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`Id_Cliente`, `Nombre`, `Apellido`, `Cedula`, `No_Tarjeta`, `Límite_Credito`, `Tipo_Persona`, `Estado`) VALUES
(1, 'jaieo', 'wewe', '454545', '545454545', 45464545, 'Fiscal', 0),
(2, 'ddfdf', 'dfd', '45435345435435', '34543545345', 1, 'Fiscal', 1),
(3, '.Jairo.', '.Munoz.', '.353465346.', '.45645645.', 456456, 'Jurídica', 1),
(4, 'yuyu', 'yuu', '45', '45', 45, 'Fiscal', 1),
(5, 'jairo', 'esemrlin', '24354354534', '54354', 54354, 'Fiscal', 1),
(6, 'ss', 'sd', '1533456', '456456', 456456, 'Fiscal', 1),
(7, 'jairo', 'hola', '565', '77885656', 564545, 'Fiscal', 1),
(8, 'sdsdf', 'sdfsdf', '545656', '4564', 45654, 'Fiscal', 0),
(9, 'danelis', 'munoz', '353541543545', '35435153', 3545435, 'Fiscal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `Id_Empleado` int(11) NOT NULL,
  `Nombre` varchar(20) DEFAULT NULL,
  `Apellido` varchar(20) DEFAULT NULL,
  `Cédula` varchar(20) DEFAULT NULL,
  `Tanda_labor` int(11) DEFAULT NULL,
  `Porciento_Comision` int(11) DEFAULT NULL,
  `Fecha_Ingreso` datetime DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`Id_Empleado`, `Nombre`, `Apellido`, `Cédula`, `Tanda_labor`, `Porciento_Comision`, `Fecha_Ingreso`, `Estado`) VALUES
(1, 'Jairo', 'Munoz', '1234567', 1, 5, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

CREATE TABLE `estados` (
  `Id_Estado` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estados`
--

INSERT INTO `estados` (`Id_Estado`, `Descripcion`) VALUES
(1, 'Activo'),
(2, 'Inactivo'),
(3, 'Rentado');

-- --------------------------------------------------------

--
-- Table structure for table `inspeccion`
--

CREATE TABLE `inspeccion` (
  `Id_Transaccion` int(11) NOT NULL,
  `Vehiculo` int(11) DEFAULT NULL,
  `Id_Cliente` int(11) DEFAULT NULL,
  `Tiene_Ralladuras` int(11) DEFAULT NULL,
  `Cantidad_Conbustible` varchar(50) DEFAULT NULL,
  `TieneGomaRepuesto` varchar(50) DEFAULT NULL,
  `TieneGato` varchar(50) DEFAULT NULL,
  `TieneRoturaCrsital` varchar(50) DEFAULT NULL,
  `EstadoGomas` varchar(50) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Empleado_inspeccion` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inspeccions`
--

CREATE TABLE `inspeccions` (
  `Id_Transaccion` int(11) NOT NULL,
  `Vehiculo` int(11) DEFAULT NULL,
  `Id_Cliente` int(11) DEFAULT NULL,
  `Tiene_Ralladuras` int(11) DEFAULT NULL,
  `Cantidad_Conbustible` varchar(30) DEFAULT NULL,
  `TieneGomaRepuesto` varchar(30) DEFAULT NULL,
  `TieneGato` varchar(30) DEFAULT NULL,
  `TieneRoturaCrsital` varchar(30) DEFAULT NULL,
  `EstadoGomas` varchar(30) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  `Empleado_inspeccion` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marcas`
--

CREATE TABLE `marcas` (
  `Id_Marca` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marcas`
--

INSERT INTO `marcas` (`Id_Marca`, `Descripcion`, `Estado`) VALUES
(1, 'Dacia', 1),
(2, 'Alfa Romeo', 1),
(3, 'Audi', 1),
(4, 'Bentley', 1),
(5, 'BMW', 1),
(6, 'Bugatti', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modelos`
--

CREATE TABLE `modelos` (
  `Id_Modelo` int(11) NOT NULL,
  `Id_Marca` int(11) DEFAULT NULL,
  `Descripcion` varchar(30) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modelos`
--

INSERT INTO `modelos` (`Id_Modelo`, `Id_Marca`, `Descripcion`, `Estado`) VALUES
(1, 2, 'Stelvio', 1),
(2, 2, 'MiTo', 1),
(3, 2, 'test', 1),
(4, 1, 'Dokker', 1),
(5, 1, 'Duster', 1),
(6, 3, 'A1', 1),
(7, 3, 'Q1', 1),
(8, 5, 'X3', 1),
(9, 5, 'Serie 7', 1),
(10, 4, 'Bentayga', 1),
(11, 4, 'Continental GT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rentaydevolucion`
--

CREATE TABLE `rentaydevolucion` (
  `RentaId` int(11) NOT NULL,
  `id_Empleado` int(11) DEFAULT NULL,
  `Id_Vehiculo` int(11) DEFAULT NULL,
  `Id_cliente` int(11) DEFAULT NULL,
  `Fecha_Renta` datetime DEFAULT NULL,
  `Fecha_Devolución` datetime DEFAULT NULL,
  `MontoXDía` int(11) DEFAULT NULL,
  `Cantidad_días` int(11) DEFAULT NULL,
  `Comentario` varchar(100) DEFAULT NULL,
  `Estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentaydevolucion`
--

INSERT INTO `rentaydevolucion` (`RentaId`, `id_Empleado`, `Id_Vehiculo`, `Id_cliente`, `Fecha_Renta`, `Fecha_Devolución`, `MontoXDía`, `Cantidad_días`, `Comentario`, `Estado`) VALUES
(1, 1, 1, 2, NULL, '2018-06-13 00:00:00', 12, 1234, 'test', 2),
(2, 1, 1, 2, NULL, '2018-06-13 00:00:00', 12, 234, 'test', 2),
(3, 1, 1, 2, '2018-06-13 00:00:00', '2018-06-13 00:00:00', 3, 34, 'test', 1),
(4, 1, 2, 5, '2018-06-13 00:00:00', '2018-06-15 00:00:00', 31, 213, 'fgxfgdfg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tanda_laboral`
--

CREATE TABLE `tanda_laboral` (
  `Id_TandaLaboral` int(11) NOT NULL,
  `Descripcion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanda_laboral`
--

INSERT INTO `tanda_laboral` (`Id_TandaLaboral`, `Descripcion`) VALUES
(1, 'Matutina'),
(2, 'Vespertina'),
(3, 'Nocturna');

-- --------------------------------------------------------

--
-- Table structure for table `tipos_combustible`
--

CREATE TABLE `tipos_combustible` (
  `Id_Combustible` int(11) NOT NULL,
  `Descripcion` varchar(30) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipos_combustible`
--

INSERT INTO `tipos_combustible` (`Id_Combustible`, `Descripcion`, `Estado`) VALUES
(1, 'Gas natural', 1),
(2, 'Acetileno', 1),
(3, 'Propano', 1),
(4, 'Gasoil', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipos_vehiculos`
--

CREATE TABLE `tipos_vehiculos` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipos_vehiculos`
--

INSERT INTO `tipos_vehiculos` (`Id`, `Descripcion`, `Estado`) VALUES
(1, 'Descripcion', 1),
(2, 'Motocicleta', 1),
(3, 'Motocarro', 1),
(4, 'Automóvil de 3 ruedas', 1),
(5, 'Bicicleta', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `Id_Usuario` int(10) NOT NULL,
  `Usuario` varchar(30) DEFAULT NULL,
  `Contrasenia` varchar(30) DEFAULT NULL,
  `Id_empleado` int(10) DEFAULT NULL,
  `Role` int(10) DEFAULT NULL,
  `Estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`Id_Usuario`, `Usuario`, `Contrasenia`, `Id_empleado`, `Role`, `Estado`) VALUES
(1, 'jairoesmerlin@hotmail.com', '1234', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuroles`
--

CREATE TABLE `usuroles` (
  `Id_Role` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehiculos`
--

CREATE TABLE `vehiculos` (
  `Id_Vehiculo` int(11) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `No_Chasis` varchar(50) DEFAULT NULL,
  `No_Motor` varchar(30) DEFAULT NULL,
  `No_Placa` varchar(30) DEFAULT NULL,
  `Tipo_Vehiculo` int(11) DEFAULT NULL,
  `Marca` int(11) DEFAULT NULL,
  `Modelo` int(11) DEFAULT NULL,
  `Tipo_Combustible` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehiculos`
--

INSERT INTO `vehiculos` (`Id_Vehiculo`, `Descripcion`, `No_Chasis`, `No_Motor`, `No_Placa`, `Tipo_Vehiculo`, `Marca`, `Modelo`, `Tipo_Combustible`, `Estado`) VALUES
(1, 'hola', '89789454', '6576576', '4545', 1, 2, 2, 1, 2),
(2, 'Carro nuevo', '4DFGDFG44DFG', 'DFGFD255', 'DGDFG554', 2, 3, 6, 3, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`Id_Cliente`);

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`Id_Empleado`);

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`Id_Estado`);

--
-- Indexes for table `inspeccion`
--
ALTER TABLE `inspeccion`
  ADD PRIMARY KEY (`Id_Transaccion`);

--
-- Indexes for table `inspeccions`
--
ALTER TABLE `inspeccions`
  ADD PRIMARY KEY (`Id_Transaccion`);

--
-- Indexes for table `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`Id_Marca`);

--
-- Indexes for table `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`Id_Modelo`);

--
-- Indexes for table `rentaydevolucion`
--
ALTER TABLE `rentaydevolucion`
  ADD PRIMARY KEY (`RentaId`);

--
-- Indexes for table `tanda_laboral`
--
ALTER TABLE `tanda_laboral`
  ADD PRIMARY KEY (`Id_TandaLaboral`);

--
-- Indexes for table `tipos_combustible`
--
ALTER TABLE `tipos_combustible`
  ADD PRIMARY KEY (`Id_Combustible`);

--
-- Indexes for table `tipos_vehiculos`
--
ALTER TABLE `tipos_vehiculos`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id_Usuario`);

--
-- Indexes for table `usuroles`
--
ALTER TABLE `usuroles`
  ADD PRIMARY KEY (`Id_Role`);

--
-- Indexes for table `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`Id_Vehiculo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `Id_Cliente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `empleados`
--
ALTER TABLE `empleados`
  MODIFY `Id_Empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `Id_Estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inspeccion`
--
ALTER TABLE `inspeccion`
  MODIFY `Id_Transaccion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inspeccions`
--
ALTER TABLE `inspeccions`
  MODIFY `Id_Transaccion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marcas`
--
ALTER TABLE `marcas`
  MODIFY `Id_Marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `modelos`
--
ALTER TABLE `modelos`
  MODIFY `Id_Modelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rentaydevolucion`
--
ALTER TABLE `rentaydevolucion`
  MODIFY `RentaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tanda_laboral`
--
ALTER TABLE `tanda_laboral`
  MODIFY `Id_TandaLaboral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipos_combustible`
--
ALTER TABLE `tipos_combustible`
  MODIFY `Id_Combustible` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tipos_vehiculos`
--
ALTER TABLE `tipos_vehiculos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuroles`
--
ALTER TABLE `usuroles`
  MODIFY `Id_Role` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `Id_Vehiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
