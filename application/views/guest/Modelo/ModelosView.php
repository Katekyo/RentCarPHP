<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Modelos</h3>
        </div>
        <br>
        <div class="row">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <div> <?php echo $form?> </div>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);"
                                        class="dropdown-toggle"
                                        data-toggle="dropdown" role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url();?>Modelos/AddModelos"><i class="material-icons">add_circle</i>Nuevo</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped
                                    table-hover dataTable js-exportable"
                                    id="mainTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($AllModelo as $key) {  ?>
                                        <tr>
                                            <td><?php echo $key->Id_Modelo?></td>
                                            <td><?php echo $key->Marca?></td>
                                            <td><?php echo $key->Modelo?></td>
                                            <td><?php echo $key->Estado ?></td>
                                            <td>
                                                <button type="button" onclick="editModelo(<?php echo $key->Id_Modelo?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">edit</i></button>
                                               <?php if ($key->Estado == 'Activo') {?>
                                                <button type="button" onclick="deleteModelo(<?php echo $key->Id_Modelo?>);" class="btn btn-danger waves-effect" ><i class="material-icons">delete</i></button>
                                               <?php }?>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </div>
            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row clearfix">
                            <div class="row clearfix js-sweetalert">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="body">
                                            <form id="FormNewModelo" >
                                                <div class="form-group form-float">
                                                    <div class="form-line"><input type="text" name="Descripcion" id="Descripcion" class="form-control" required>
                                                        <input type='hidden' name='id' id='id'  />
                                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                                    </div>
                                                    <div class="form-float">
                                                        <select class="form-control show-tick" name="idMarca" id="Modelo" required>
                                                            <option value="0">Seleccione Marca</option>
                                                            <?php foreach ($AllMarcas as $key) {?>
                                                                <option value="<?php echo $key->Id_Marca?>"><?php echo $key->Descripcion?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="checkbox" name="Activo" id="Activo" class="filled-in" > 
                                                <label for="Activo">Activo</label>
                                                <br>
                                                <button type="button" class="btn btn-primary waves-effect" id="btn_NewModelo" data-type="ajax-loader">Guardar</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>