<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Clientes</h3>
        </div>
        <br>
        <div class="row">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <div> <?php echo $form?> </div>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);"
                                        class="dropdown-toggle"
                                        data-toggle="dropdown" role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url();?>Clientes/AddCliente"><i
                                                    class="material-icons">add_circle</i>Nuevo</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped
                                    table-hover dataTable js-exportable"
                                    id="mainTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Cedula</th>
                                            <th>No de Tarjeta</th>
                                            <th>Límite de Credito</th>
                                            <th>Tipo_Persona</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Cedula</th>
                                            <th>No de Tarjeta</th>
                                            <th>Límite de Credito</th>
                                            <th>Tipo_Persona</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($AllClient as $key) {  ?>
                                        <tr>
                                            <td><?php echo $key->Id_Cliente?></td>
                                            <td><?php echo $key->Nombre?></td>
                                            <td><?php echo $key->Apellido?></td>
                                            <td><?php echo $key->Cedula?></td>
                                            <td><?php echo $key->No_Tarjeta?></td>
                                            <td><?php echo $key->Límite_Credito?></td>
                                            <td><?php echo $key->Tipo_Persona?></td>
                                            <td><?php echo $key->Estado == 1? 'Activo':'inactivo'  ?></td>
                                            <td>
                                                <button type="button" onclick="editClient(<?php echo $key->Id_Cliente?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">edit</i></button>
                                               <?php if ($key->Estado == 1) {?>
                                                <button type="button" onclick="deleteClient(<?php echo $key->Id_Cliente?>);" class="btn btn-danger waves-effect" ><i class="material-icons">delete</i></button>
                                               <?php }?>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </div>
            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row clearfix">
                                <div class="row clearfix js-sweetalert">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="body">
                                                <form id="FormNewClient" >
                                                    <div class="form-group form-float">
                                                    <label class="form-label">Nombre</label>
                                                        <div class="form-line"><input type="text" name="nombre" id="nombre" class="form-control" required>
                                                            <input type='hidden' name='id' id='id'  />
                                                            <input type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-float">
                                                    <label class="form-label">Apellido</label>
                                                        <div class="form-line">
                                                            <input type="text" name="apellido"
                                                                id="apellido" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-float">
                                                    <label class="form-label">Cedula</label>
                                                        <div class="form-line">
                                                            <input type="text" name="cedula"
                                                                id="Cedula" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="form-line">
                                                            <input type="text" name="credit" class="form-control 
                                                                credit-card" id="credit" placeholder="Credit Card Ex: 0000 0000 0000 0000" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-float">
                                                    <label class="form-label">Límite de Credito</label>
                                                        <div class="form-line">
                                                            <input type="text" name="Límite_Credito"
                                                                id="Límite_Credito"
                                                                class="form-control" required>
                                                                <div id ="container" > </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="form-control show-tick" name="tipo" id="tipopersona" required>
                                                            <option value="0">Seleccione...</option>
                                                            <option value="Fiscal">Fiscal</option>
                                                            <option value="Jurídica">Jurídica</option>
                                                        </select>
                                                    </div>
                                                    <input type="checkbox" name="Activo" id="Activo" class="filled-in" > 
                                                    <label for="Activo">Activo</label>
                                                    <br>
                                                    <div class =" form-float pull-right">
                                                    <button type="button" class="btn btn-primary  waves-effect" id="btn_NewClient"  data-type="ajax-loader">Guardar</button>
                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>