<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Nuevo Cliente</h3>
        </div>
        <br>
        <!-- Widgets -->
        <div class="row clearfix">
            <div class="row clearfix js-sweetalert">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form id="FormNewClient" >
                                <div class="form-group form-float">
                                    <div class="form-line"><input type="text" name="nombre" id="nombre" class="form-control" required>
                                        <input class='validate' type='hidden' name='Url' id='Url' value="<?php echo base_url()?>"  />
                                        <label class="form-label">Nombre</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="apellido"
                                            id="apellido" class="form-control" required>
                                        <label class="form-label">Apellido</label>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="cedula"
                                            id="Cedula" class="form-control" required>
                                        <label class="form-label">Cedula</label>
                                    </div>
                                </div>

                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="credit"  id="credit" class="form-control
                                            credit-card" placeholder="Credit Card Ex: 0000 0000 0000 0000" required>
                                    </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="Límite_Credito" id="Límite_Credito" class="form-control" required>
                                        <label class="form-label">Límite de Credito</label>
                                        <div id ="container" > </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control show-tick" name="tipo" required>
                                        <option value="0">Seleccione...</option>
                                        <option value = "Fiscal">Fiscal</option>
                                        <option value = "Jurídica">Jurídica</option>
                                    </select>
                                </div>
                                <input type="checkbox" name="Activo"
                                    id="remember_me_2" class="filled-in" > 
                                <label for="remember_me_2">Activo</label>
                                <br>
                                <button type="button" class="btn btn-primary
                                    waves-effect" id="btn_NewClient"
                                    data-type="ajax-loader">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 