</section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery/jquery.min.js"></script>


    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <!-- Select Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <!-- Editable Table Plugin Js
    <script src="<?php echo base_url();?>plantilla/plugins/editable-table/mindmup-editabletable.js"></script>
    <script src="<?php echo base_url();?>plantilla/js/pages/tables/editable-table.js"></script> -->
    <script src="<?php echo base_url();?>plantilla/js/pages/ui/modals.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-validation/jquery.validate.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <script src="<?php echo base_url();?>plantilla/js/pages/tables/jquery-datatable.js"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/node-waves/waves.js"></script>
    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-steps/jquery.steps.js"></script>
    <script src="<?php echo base_url();?>plantilla/js/pages/forms/form-wizard.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<?php echo base_url();?>plantilla/plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    
    <script src="<?php echo base_url();?>plantilla/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/flot-charts/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>plantilla/plugins/editable-table/mindmup-editabletable.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url();?>plantilla/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url();?>plantilla/js/admin.js"></script>
    <script src="<?php echo base_url();?>plantilla/js/script.js"></script>
    <!-- Demo Js -->
    <script src="<?php echo base_url();?>plantilla/js/pages/index.js"></script>
 
    <!-- Demo Js -->
    <script src="<?php echo base_url();?>plantilla/js/demo.js"></script>



</body>

</html>