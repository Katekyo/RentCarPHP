<html>

<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
      background-image: url("<?php base_url()?>plantilla/images/back.jpg");
      
    }
    main {
      flex: 1 0 auto;  
      background: rgba(255,255,255,0.7);
          
    }
    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #e91e63;
      box-shadow: none;
    }
    .ButtomColor{
        background-color:#29b6f6;
    }
    .TextColor{
        color: black;
    }
  </style>
</head>

<body >
  <main>
    <center>
      <!-- <img class="responsive-img" style="width: 250px;" src="https://i.imgur.com/ax0NCsK.gif" /> -->
      <div class="section"></div>

      <h5 >Iniciar Sesion</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

          <form class="col s12" id ="FormLogin">
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='email' name='email' id='email' require/>
                <input class='validate' type='hidden' name='Url' id='Url' value ="<?php echo base_url()?>" />                
                <label for='email'>Correo</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' require />
                <label for='password'>Contraseña</label>
              </div>
              <label style='float: right;'>
								<a class='pink-text' href='#!'><b>Forgot Password?</b></a>
							</label>
            </div>
            <div id='EmailValidation' style="color:red;" ></div>
            <br />
            <center>
              <div class='row'>
                <button type='button' name='btn_login' class='col s12 btn btn-large ButtomColor' id ="btn_login">Login</button>
              </div>
            </center>
          </form>
        </div>
      </div>
      <a href="#!" class = "TextColor">Create account</a>
    </center>

    <div class="section"></div>
    <div class="section"></div>
  </main>
  <script src="<?= base_url()?>plantilla/plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url()?>plantilla/js/script.js"></script>
  <script src="<?= base_url()?>plantilla/plugins/materialize-css/js/materialize.js"></script>
</body>

</html>