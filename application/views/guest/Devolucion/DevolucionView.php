<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h3>Devolucion de Vehiculos</h3>
        </div>
        <br>
        <div class="row">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <!-- <div> <?php echo $form?> </div> -->
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);"
                                        class="dropdown-toggle"
                                        data-toggle="dropdown" role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url();?>Vehiculos/AddVehiculo"><i class="material-icons">add_circle</i>Nuevo</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped
                                    table-hover dataTable js-exportable"
                                    id="mainTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Descripcion</th>
                                            <th>No. Chasis</th>
                                            <th>No. Motor</th>
                                            <th>No. Placa</th>
                                            <th>Tipo de Vehiculo</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Tipo de Combustible</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Descripcion</th>
                                            <th>No. Chasis</th>
                                            <th>No. Motor</th>
                                            <th>No. Placa</th>
                                            <th>Tipo de Vehiculo</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Tipo de Combustible</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($RentVehiculos as $key) {  ?>
                                        <tr>
                                            <td><?php echo $key->Id_Vehiculo?></td>
                                            <td><?php echo $key->Descripcion?></td>
                                            <td><?php echo $key->No_Chasis?></td>
                                            <td><?php echo $key->No_Motor?></td>
                                            <td><?php echo $key->No_Placa?></td>
                                            <td><?php echo $key->tipovehiculo?></td>
                                            <td><?php echo $key->marca?></td>
                                            <td><?php echo $key->modelo?></td>
                                            <td><?php echo $key->combustible?></td>
                                            <td><?php echo $key->DescripEstados ?></td>
                                            <td>
                                                <button type="button" onclick="rentarVehiculo(<?php echo $key->Id_Vehiculo?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">eject</i>Devolver</button>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </div>
            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <input type="text" name="id_vehiculo" id="id_vehiculo" class="form-control" required>
                            <h4 class="modal-title" id="largeModalLabel">Seleccione Cliente</h4>
                        </div>
                        <div class="modal-body">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped
                                    table-hover dataTable js-exportable"
                                    id="mainTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Cedula</th>
                                            <th>No de Tarjeta</th>
                                            <th>Límite de Credito</th>
                                            <th>Tipo_Persona</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Cedula</th>
                                            <th>No de Tarjeta</th>
                                            <th>Límite de Credito</th>
                                            <th>Tipo_Persona</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach ($AllClient as $key) {  ?>
                                        <tr>
                                            <td><?php echo $key->Id_Cliente?></td>
                                            <td><?php echo $key->Nombre?></td>
                                            <td><?php echo $key->Apellido?></td>
                                            <td><?php echo $key->Cedula?></td>
                                            <td><?php echo $key->No_Tarjeta?></td>
                                            <td><?php echo $key->Límite_Credito?></td>
                                            <td><?php echo $key->Tipo_Persona?></td>
                                            <td><?php echo $key->Estado == 1? 'Activo':'inactivo'  ?></td>
                                            <td>
                                                <button type="button" onclick="SelectClienteDevolucion(<?php echo $key->Id_Cliente?>);" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#largeModal"><i class="material-icons">check_circle</i></button>
               
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <!-- <button type="button" class="btn btn-primary  waves-effect" id="btn_NewVehiculo"  data-type="ajax-loader">Guardar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>-->
                    </div>
                </div>
             </div>
            </div>

                <div class="modal fade" id="largeModal3" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Inspeccion</h4>
                        </div>

                        <form id ="RentarVheiculo">
                        <div class="modal-body">
                            <input  type='text' name="id2" id="id2" class="form-control" required>
                            <input  type='text' name="id_vehiculo1" id="id_vehiculo1" class="form-control" required>
                            <input  type='text' name='Url' id='Url' value="<?php echo base_url()?>"  />

                            <div>
                              <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="body">
                                        <form>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Ralladuras</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_ralladuras" class="filled-in">
                                                    <label for="si_ralladuras">Si</label>
                                                    <input type="checkbox" id="no_ralladuras" class="filled-in">
                                                    <label for="no_ralladuras">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Cantidad Combustible</p>
                                                    <div  class="form-inline">
                                                    <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" name ="ig_checkbox" id="ig_checkbox">
                                                    <label for="ig_checkbox"></label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" name ="ig_checkbox2" id="ig_checkbox2">
                                                    <label for="ig_checkbox2"></label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" name ="ig_checkbox3" id="ig_checkbox3">
                                                    <label for="ig_checkbox3"></label>
                                                   </span>
                                                   <span class="input-group-addon">
                                                    <input type="checkbox" class="with-gap" name ="ig_checkbox4" id="ig_checkbox4">
                                                    <label for="ig_checkbox4"></label>
                                                   </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Goma respuesta</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_Goma" name ="si_Goma" class="filled-in">
                                                    <label for="si_Goma">Si</label>
                                                    <input type="checkbox" id="no_Goma" name ="si_Goma" class="filled-in">
                                                    <label for="no_Goma">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene Gato</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_Gato" name ="si_Gato" class="filled-in">
                                                    <label for="si_Gato">Si</label>
                                                    <input type="checkbox" id="no_Gato" name ="si_Gato" class="filled-in">
                                                    <label for="no_Gato">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <p>Tiene roturas cristal</p>
                                                    <div  class="form-inline">
                                                    <input type="checkbox" id="si_cristal" name ="si_cristal" class="filled-in">
                                                    <label for="si_cristal">Si</label>
                                                    <input type="checkbox" id="no_cristal" name ="no_cristal" class="filled-in">
                                                    <label for="no_cristal">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                 <p>Estado gomas </p>
                                                <div class="form-line">
                                                   <p>gomas 1</p>
                                                    <div>
                                                    <input type="checkbox" name = "gomasBuena1" id="Estado_gomasBuena1" class="filled-in">
                                                    <label for="Estado_gomasBuena1">Buena Condicion </label>
                                                    <input type="checkbox" name = "gomasMala1" id="Estado_gomasMala1" class="filled-in">
                                                    <label for="Estado_gomasMala1">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 2</p>
                                                    <div>
                                                    <input type="checkbox" name = "gomasBuena2" id="Estado_gomasBuena2" class="filled-in">
                                                    <label for="Estado_gomasBuena2">Buena Condicion </label>
                                                    <input type="checkbox" name = "gomasMala2" id="Estado_gomasMala2" class="filled-in">
                                                    <label for="Estado_gomasMala2">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 3</p>
                                                    <div>
                                                    <input type="checkbox" name = "gomasBuena3" id="Estado_gomasBuena3" class="filled-in">
                                                    <label for="Estado_gomasBuena3">Buena Condicion </label>
                                                    <input type="checkbox" name = "gomasMala3" id="Estado_gomasMala3" class="filled-in">
                                                    <label for="Estado_gomasMala3">Mala Condicion</label>
                                                    </div>
                                                    <p>gomas 4</p>
                                                    <div>
                                                    <input type="checkbox" name = "gomasBuena4" id="Estado_gomasBuena4" class="filled-in">
                                                    <label for="Estado_gomasBuena4">Buena Condicion </label>
                                                    <input type="checkbox" name = "gomasMala4" id="Estado_gomasMala4" class="filled-in">
                                                    <label for="Estado_gomasMala4">Mala Condicion</label>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <button type="button" class="btn btn-success m-t-15 waves-effect terminar2"><i class="material-icons">check</i> Terminar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
</section>