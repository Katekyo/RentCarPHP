<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		$this->load->model("ModelosModel");
		
	}
	public function index()
	{
        $user_data = $this->session->userdata('login');
        $id =$this->input->post('all');
		$all = $id;
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Modelos');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,$Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

            if ($all == 1) {
                $data =  array('AllModelo' => $this->ModelosModel->getAllModelo($all),'AllMarcas' => $this->ModelosModel->getAllMarcas(),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value='1'>Activos</option>
                            <option value='2'>Todos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Modelo/ModelosView',$data );
            }else{
                $data =  array('AllModelo' => $this->ModelosModel->getAllModelo($all),'AllMarcas' => $this->ModelosModel->getAllMarcas(),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value='2'>Todos</option>
                            <option value='1'>Activos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Modelo/ModelosView',$data );
            }

			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
        }
        
    }
    public function AddModelos()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Nuevo Modelo');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
            #Plantilla fin
            
            $data =  array('AllMarcas' => $this->ModelosModel->getAllMarcas());
			$this->load->view('guest/Modelo/NuevoModeloView',$data );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
    }
    public function SaveModelo()
	{
		$id =$this->input->post('id');
		$Descripcion =$this->input->post('Descripcion');
		$idMarca =$this->input->post('idMarca');
		$Activo =$this->input->post('Activo');

		$result = $this->ModelosModel->SaveModelo($id,$Descripcion,$idMarca,$Activo);
		echo $result;
	}
	public function UpdateModelo()
	{
		$id = $this->input->post('id');
		$result = $this->ModelosModel->UpdateModelo($id);
		echo $result;
	}
    public function SelectModelo()
	{
		$id = $this->input->post('id');
		$result = $this->ModelosModel->getModeloById($id);
		echo json_encode($result);
	}
}
