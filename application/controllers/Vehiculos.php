<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculos extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
       $this->load->model("VehiculoModel");
		
	}
	public function index()
	{
        $user_data = $this->session->userdata('login');
        $id =$this->input->post('all');
		$all = $id;
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Marcas');
			$this->load->view('guest/head',$data);
			//Descripcion
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,$Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin

            if ($all == 1) {
                $data =  array('Vehiculos' => $this->VehiculoModel->getAllVehiculos($all),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value='1'>Activos</option>
                            <option value='2'>Todos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Vehiculo/VehiculosView',$data );
            }else{
                $data =  array('Vehiculos' => $this->VehiculoModel->getAllVehiculos($all),
                        'form' => "<form id='target' method='post' action=''>
                        <select class='show-tick' name='all' id ='all' required>
                            <option value='2'>Todos</option>
                            <option value='1'>Activos</option>
                        </select>
                    </form>");
                $this->load->view('guest/Vehiculo/VehiculosView',$data );
            }

			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
        }
        
    }
    public function AddVehiculo()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Nuevo Vehiculo');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			
			$id = $this->input->post('idMarca');

			$data =  array(
			'NombreCompleto' => $NombreCompleto,
			'Correo' => $Correo,
			'active' => 'Mantenimientos',
			'modelo' => $this->VehiculoModel->getAllModelo($id),
			'TipoConbustible' => $this->VehiculoModel->getAllTipoConbustible(),
			'TipoVehiculo' => $this->VehiculoModel->getAllTipoVehiculos(),
			'AllMarcas' => $this->VehiculoModel->getAllMarcas(1)
		);

			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
            #Plantilla fin

			$this->load->view('guest/Vehiculo/NuevoVehiculosView',$data);


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
    }
    public function SaveVehiculoRent()
	{
		$id =$this->input->post('id');
		$Descripcion =$this->input->post('Descripcion');
		$No_Chasis =$this->input->post('No_Chasis');
		$No_Motor =$this->input->post('No_Motor');
		$No_Placa =$this->input->post('No_Placa');
		$Tipo_Vehiculo =$this->input->post('Tipo_Vehiculo');
		$idModelo =$this->input->post('idModelo');
		$idMarca =$this->input->post('idMarca');
		$IdCombustible =$this->input->post('IdCombustible');
		$Activo =$this->input->post('Activo');
		$result = $this->VehiculoModel->SaveVehiculo($id,$Descripcion,$No_Chasis,$No_Motor,$No_Placa,$Tipo_Vehiculo,$idModelo,$idMarca,$IdCombustible,$Activo);
		echo $result;
	}
	public function UpdateMarca()
	{
		$id = $this->input->post('id');
		$this->load->model("VehiculoModel");
		$result = $this->VehiculoModel->UpdateMarca($id);
		echo $result;
	}
    // public function SelectMarca()
	// {
	// 	$id = $this->input->post('id');
	// 	$this->load->model("VehiculoModel");
	// 	$result = $this->VehiculoModel->getMarcaById($id);
	// 	echo json_encode($result);
	// }
}
