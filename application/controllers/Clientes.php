<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*'); 
		
	}
	public function index()
	{
		$user_data = $this->session->userdata('login');
		$id =$this->input->post('all');
		$all = $id;
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Clientes');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin
			if ($all == 1) {
				$this->load->model("ClientModel");
				$data =  array('AllClient' => $this->ClientModel->getAllClient($all),
						'form' => "<form id='target' method='post' action=''>
						<select class='show-tick' name='all' id ='all' required>
							<option value=''>Activos</option>
							<option value='2'>Todos</option>
						</select>
					</form>");
				$this->load->view('guest/Cliente/ListaClienteView',$data );
			}else{
				$this->load->model("ClientModel");
				$data =  array('AllClient' => $this->ClientModel->getAllClient($all),
						'form' => "<form id='target' method='post' action=''>
						<select class='show-tick' name='all' id ='all' required>
							<option value='2'>Todos</option>
							<option value='1'>Activos</option>
						</select>
					</form>");
				$this->load->view('guest/Cliente/ListaClienteView',$data );
			}



			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function AddCliente()
	{
		$user_data = $this->session->userdata('login');
		if ($user_data == true) {
			#Plantilla inicios
			$data = array('titulo' => 'Clientes');
			$this->load->view('guest/head',$data);
			$NombreCompleto = $this->session->userdata('Nombre');
			$Correo = $this->session->userdata('email');
			$data =  array('NombreCompleto' => $NombreCompleto,'Correo' => $Correo,'active' => 'Mantenimientos');
			$this->load->view('guest/nav');
			$this->load->view('guest/header',$data);
			#Plantilla fin
			$this->load->view('guest/Cliente/NuevoClienteView' );


			#Plantilla inicios
			$this->load->view('guest/footer');
			#Plantilla fin
		}else{
			$data =  array('test' => "");
			$this->load->view('guest/LoginView',$data);
		}
	}
	public function SaveClinete()
	{
		$id =$this->input->post('id');
		$nombre =$this->input->post('nombre');
		$apellido =$this->input->post('apellido');
		$cedula =$this->input->post('cedula');
		$credit =$this->input->post('credit');
		$Límite_Credito =$this->input->post('Límite_Credito');
		$tipo = $this->input->post('tipo');
		$Activo =$this->input->post('Activo');

		$this->load->model("ClientModel");
		$result = $this->ClientModel->SaveClient($id,$nombre,$apellido,$cedula,$credit,$Límite_Credito,$tipo,$Activo);
		echo $result;
	}
	public function UpdateClient()
	{
		$id = $this->input->post('id');
		$this->load->model("ClientModel");
		$result = $this->ClientModel->UpdateClient($id);
		echo $result;
	}
	public function SelectClient()
	{
		$id = $this->input->post('id');
		$this->load->model("ClientModel");
		$result = $this->ClientModel->getClientById($id);
		echo json_encode($result);
	}
}
