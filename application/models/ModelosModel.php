<?php
class ModelosModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveModelo($id,$Descripcion,$idMarca,$estado)
    {
        $status = $estado == "on" ? 1 : 2;
        
        if ($id > 0) {
            $array = array(
                'Descripcion' => $Descripcion,
                'Id_Marca' => $idMarca,
                'Estado' => $status
        );
            $this->db->set($array);
            $this->db->where('Id_Modelo', $id);
            $query =  $this->db->update('modelos');
            return $query;
        }else{
            $data = array(
                'Descripcion' => $Descripcion,
                'Id_Marca' => $idMarca,
                'Estado' => $status
                );
            $query =   $this->db->insert('modelos', $data);
            return $query;
        }

    }

    public function getAllMarcas()
    {
        $this->db->select('Id_Marca,Descripcion, Estado');
        $this->db->from('marcas');
        $this->db->where('Estado', 1);
        $query = $this->db->get(); 
        return $query->result();
    }

    public function getAllModelo($all)
    {
        if ($all == 1) {
            $query =  $this->db->query('SELECT m.Id_Modelo,mc.Descripcion Marca,m.Descripcion Modelo, e.Descripcion Estado FROM Modelos m JOIN estados e ON (m.Estado = e.Id_Estado)
            JOIN Marcas mc ON (mc.Id_Marca = m.Id_Marca) AND e.Id_Estado = '.$all);
            return $query->result();
        }else{
            $query = $this->db->query("SELECT m.Id_Modelo,mc.Descripcion Marca,m.Descripcion Modelo, e.Descripcion Estado FROM Modelos m JOIN estados e ON (m.Estado = e.Id_Estado)
            JOIN Marcas mc ON (mc.Id_Marca = m.Id_Marca)");
            return $query->result();
        }

    }

    public function getModeloById($id)
    {
                 $this->db->select('Id_Modelo,Id_Marca, Descripcion,Estado');
                 $this->db->from('modelos');
                 $this->db->where('Id_Modelo', $id);
                $query = $this->db->get(); 
                return $query->result();
    }
    public function UpdateModelo($id)
    {

        try{
            $this->db->set('Estado', 2,false);
            $this->db->where('Id_Modelo', $id);
            $query =  $this->db->update('modelos');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

