<?php
class VehiculoModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveVehiculo($id,$Descripcion,$No_Chasis,$No_Motor,$No_Placa,$Tipo_Vehiculo,$idModelo,$idMarca,$IdCombustible,$estado)
    {
        $status = $estado == "on" ? 1 : 2;
        
        if ($id > 0) {
            $array = array(
                'Descripcion' => $Descripcion,
                'No_Chasis' => $No_Chasis,
                'No_Motor' => $No_Motor,
                'No_Placa' => $No_Placa,
                'Tipo_Vehiculo' => $Tipo_Vehiculo,
                'Marca' => $idMarca,
                'Modelo' => $idModelo,
                'Tipo_Combustible' => $IdCombustible,
                'Estado' => $status
        );
            $this->db->set($array);
            $this->db->where('Id_Vehiculo', $id);
            $query =  $this->db->update('vehiculos');
            return $query;
        }else{
            $array = array(
                'Descripcion' => $Descripcion,
                'No_Chasis' => $No_Chasis,
                'No_Motor' => $No_Motor,
                'No_Placa' => $No_Placa,
                'Tipo_Vehiculo' => $Tipo_Vehiculo,
                'Marca' => $idMarca,
                'Modelo' => $idModelo,
                'Tipo_Combustible' => $IdCombustible,
                'Estado' => $status
        );
            $query =   $this->db->insert('vehiculos', $array);
            return $query;
        }

    }

    public function getAllVehiculos($all)
    {
        if ($all == 1) {
        $query =  $this->db->query('SELECT e.Descripcion descriEstado,v.Id_Vehiculo,v.Descripcion,v.No_Chasis,v.No_Motor,v.No_Placa,e.Id_Estado estado ,md.Descripcion modelo,m.`Descripcion` marca,t.Descripcion combustible,tv.Descripcion tipovehiculo FROM vehiculos v 
        JOIN estados e ON (e.Id_Estado = v.Estado)
        JOIN marcas m ON (m.Id_Marca = v.Marca)
        JOIN modelos md ON (md.Id_Modelo = v.Modelo)
        JOIN tipos_combustible t ON (t.Id_Combustible = v.Tipo_Combustible)
        JOIN tipos_vehiculos tv ON (tv.Id = v.Tipo_Vehiculo)
        
        WHERE  e.Id_Estado = '. $all);
        return $query->result();
        
        }else{
            $query =  $this->db->query('SELECT e.Descripcion descriEstado,v.Id_Vehiculo,v.Descripcion,v.No_Chasis,v.No_Motor,v.No_Placa,e.Id_Estado estado ,md.Descripcion modelo,m.`Descripcion` marca,t.Descripcion combustible,tv.Descripcion tipovehiculo FROM vehiculos v 
            JOIN estados e ON (e.Id_Estado = v.Estado)
            JOIN marcas m ON (m.Id_Marca = v.Marca)
            JOIN modelos md ON (md.Id_Modelo = v.Modelo)
            JOIN tipos_combustible t ON (t.Id_Combustible = v.Tipo_Combustible)
            JOIN tipos_vehiculos tv ON (tv.Id = v.Tipo_Vehiculo)
            ');
            return $query->result();
        }
    }
    public function getAllMarcas($all)
    {
 
            $this->db->select('Id_Marca,Descripcion, Estado');
            $this->db->from('marcas');
            $query = $this->db->get(); 
            return $query->result();

    }
    public function getAllTipoConbustible()
    {
            $this->db->select('Id_Combustible,Descripcion, Estado');
            $this->db->from('tipos_combustible');
            $this->db->where('Estado', 1);
            $query = $this->db->get(); 
            return $query->result();
    }
    public function getAllModelo($id)
    {
        if ($id > 0) {
            $query =  $this->db->query('SELECT m.Id_Modelo,mc.Descripcion Marca,m.Descripcion Modelo,m.Id_Modelo, e.Descripcion Estado FROM Modelos m JOIN estados e ON (m.Estado = e.Id_Estado)
            JOIN Marcas mc ON (mc.Id_Marca = m.Id_Marca) AND m.Id_Marca ='.$id);
            return $query->result();
        }
   }
   public function getAllTipoVehiculos()
   {
           $this->db->select('Id,Descripcion, Estado');
           $this->db->from('tipos_vehiculos');
           $this->db->where('Estado', 1);
           $query = $this->db->get(); 
           return $query->result();
   }
    // public function getModeloById($id)
    // {
    //              $this->db->select('Id_Modelo,Id_Marca, Descripcion,Estado');
    //              $this->db->from('modelos');
    //              $this->db->where('Id_Modelo', $id);
    //             $query = $this->db->get(); 
    //             return $query->result();
    // }
    public function UpdateVehiculo($id)
    {

        try{
            $this->db->set('Estado', 3,false);
            $this->db->where('Id_Vehiculo', $id);
            $query =  $this->db->update('vehiculos');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
    public function UpdateVehiculo2($id)
    {

        try{
            $this->db->set('Estado', 1,false);
            $this->db->where('Id_Vehiculo', $id);
            $query =  $this->db->update('vehiculos');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

