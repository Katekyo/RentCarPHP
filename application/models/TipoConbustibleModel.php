<?php
class TipoConbustibleModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveTipoConbustible($id,$Descripcion,$estado)
    {
        $status = $estado == "on" ? 1 : 2;
        
        if ($id > 0) {
            $array = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
        );
            $this->db->set($array);
            $this->db->where('Id_Combustible', $id);
            $query =  $this->db->update('tipos_combustible');
            return $query;
        }else{
            $data = array(
                'Descripcion' => $Descripcion,
                'Estado' => $status
                );
            $query =   $this->db->insert('tipos_combustible', $data);
            return $query;
        }

    }

    public function getAllTipoConbustible($all)
    {
        if ($all == 1) {
            $this->db->select('Id_Combustible,Descripcion, Estado');
            $this->db->from('tipos_combustible');
            $this->db->where('Estado', 1);
            $query = $this->db->get(); 
            return $query->result();
        }else{
            $this->db->select('Id_Combustible,Descripcion, Estado');
            $this->db->from('tipos_combustible');
            $query = $this->db->get(); 
            return $query->result();
        }

    }

    public function getTipoConbustiblesById($id)
    {
                 $this->db->select('Id_Combustible,Descripcion, Estado');
                 $this->db->from('tipos_combustible');
                 $this->db->where('Id_Combustible', $id);
                $query = $this->db->get(); 
                return $query->result();
    }
    public function UpdateTipoConbustible($id)
    {

        try{
            $this->db->set('Estado', 2,false);
            $this->db->where('Id_Combustible', $id);
            $query =  $this->db->update('tipos_combustible');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

