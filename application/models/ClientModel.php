<?php
class ClientModel extends CI_Model{
    function __construct()
    {
     // Llamando al contructor del Modelo
     parent::__construct();
    }

    function SaveClient($id,$nombre,$apellido,$cedula,$credit,$limit,$tipo,$estado)
    {
        if ($id > 0) {
            $array = array(
                'Nombre' => $nombre,
                'Apellido' => $apellido,
                'Cedula' => $cedula,
                'No_Tarjeta' => $credit,
                'Límite_Credito' => $limit,
                'Tipo_Persona' => $tipo,
                'Estado' => $estado == 'on' ? 1 : 0
        );
            $this->db->set($array);
            $this->db->where('Id_Cliente', $id);
            $query =  $this->db->update('clientes');
            return $query;
        }else{
            $status = $estado == "on" ? 1 : 2;
            $data = array(
                'Nombre' => $nombre,
                'Apellido' => $apellido,
                'Cedula' => $cedula,
                'No_Tarjeta' => $credit,
                'Límite_Credito' => $limit,
                'Tipo_Persona' => $tipo,
                'Estado' => $status
                );
            $query =   $this->db->insert('clientes', $data);
            return $query;
        }

    }

    public function getAllClient($all)
    {
        if ($all == 1) {
            $this->db->select('Id_Cliente,Nombre, Apellido,Cedula, No_Tarjeta, Límite_Credito, Tipo_Persona,Estado');
            $this->db->from('clientes');
            $this->db->where('Estado', 1);
            $query = $this->db->get(); 
            return $query->result();
        }else{
            $this->db->select('Id_Cliente,Nombre, Apellido,Cedula, No_Tarjeta, Límite_Credito, Tipo_Persona,Estado');
            $this->db->from('clientes');
            $query = $this->db->get(); 
            return $query->result();
        }

    }
    public function getClientById($id)
    {
                 $this->db->select('Id_Cliente,Nombre, Apellido,Cedula, No_Tarjeta, Límite_Credito, Tipo_Persona,Estado');
                 $this->db->from('clientes');
                 $this->db->where('Id_Cliente', $id);
        $query = $this->db->get(); 
        return $query->result();
    }
    public function UpdateClient($id)
    {

        try{
            $this->db->set('Estado', 2,false);
            $this->db->where('Id_Cliente', $id);
            $query =  $this->db->update('clientes');
            return $query;
           
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

    }
}

